$(() => {
    $("#palette").click(() => {
        $('.descript-wrapper').hide();
        $('#palette-description').show();
    });


    $("#karkarych").click(() => {
        $('.descript-wrapper').hide();
        $('#kojuhar-description').show();
    });

    $("#plebei").click(() => {
        $('.descript-wrapper').hide();
        $('#plebei-description').show();
    });

    $("#magician").click(() => {
        $('.descript-wrapper').hide();
        $('#magician-description').show();
    });

    $(".close").click(() => {
        $('.descript-wrapper').hide();
        $('.selector-blyad').hide();
    });

    $("#knopkablyad").click(() => {
        $('.selector-blyad').hide();
        $('#jinel-selector').show();
    });

});
